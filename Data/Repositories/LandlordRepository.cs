﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using PulseTestForDevs.Models;


namespace PulseTestForDevs.Data
{
    public class LandlordRepository
    {
        // Connection String
        private string Constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        /// <summary>
        /// Insert Landlord
        /// </summary>
        public void Insert(Landlord Landlord)
        {
            using (var connection = new SqlConnection(Constr))
            {
                connection.Execute(@"Insert into Landlord(Title, FirstName, LastName, Email)
                                                   Values(@Title, @FirstName, @LastName, @Email)",
                                                   Landlord);
            }
        }

        /// <summary>
        /// Select Landlord
        /// </summary>
        public Landlord Select(int Id)
        {
            using (var connection = new SqlConnection(Constr))
            {
                return connection.QuerySingle<Landlord>(@"Select * From Landlord", new { Id });
            }
        }

        /// <summary>
        /// Select All Landlords
        /// </summary>
        public IEnumerable<Landlord> SelectAll()
        {
            using (var connection = new SqlConnection(Constr))
            {
                return connection.Query<Landlord>(@"Select * From Landlord");
            }
        }

        /// <summary>
        /// Delete Landlord
        /// </summary>
        public void Delete(int Id)
        {
            using (var connection = new SqlConnection(Constr))
            {
                connection.Execute(@"Delete From Landlord Where Id = @Id", new { Id });
            }
        }


    }
}