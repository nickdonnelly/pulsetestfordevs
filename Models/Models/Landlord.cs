﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PulseTestForDevs.Models
{
    public class Landlord
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        public string Title { get; set; }

        [Required(ErrorMessage = "First Name Required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name Required")]
        public string LastName { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "Email Required")]
        public string Email { get; set; }
    }
}


