﻿using System;

namespace PulseTestForDevs.Models
{
    public class PropertyApplicant
    {
        public int PropertyId { get; set; }
        public int ApplicantId { get; set; }
    }
}
