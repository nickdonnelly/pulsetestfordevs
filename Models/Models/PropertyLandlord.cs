﻿using System;

namespace PulseTestForDevs.Models
{
    public class PropertyLandlord
    {
        public int PropertyId { get; set; }
        public int LandlordId { get; set; }
    }
}
