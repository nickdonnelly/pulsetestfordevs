﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PulseTestForDevs.Data;
using PulseTestForDevs.Models;

namespace PulseTestForDevs.Controllers
{
    public class LandlordController : Controller
    {
        private LandlordRepository LandlordRepo = new LandlordRepository();

        // GET: Landlord
        public ActionResult Index()
        {
            var Landlords = LandlordRepo.SelectAll();

            return View(Landlords);
        }

        //// GET: Landlord/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Landlord/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Landlord/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Landlord Landlord)
        {
            if (ModelState.IsValid)
            {
                // Insert Landlord
                LandlordRepo.Insert(Landlord);

                // PRG
                return RedirectToAction("Index");
            }
            else
            {
                // Return error to user
                return View();
            }
        }

        // GET: Landlord/Edit/5
        public ActionResult Edit(int Id)
        {
            return View();
        }

        // POST: Landlord/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int Id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Landlord/Delete/5
        public ActionResult Delete(int Id)
        {
            var Landlord = LandlordRepo.Select(Id);

            return View(Landlord);
        }

        // Delete selected
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int Id, IFormCollection collection)
        {
            //try
            //{
                // Delete in db
                LandlordRepo.Delete(Id);

                // PRG to Index
                return RedirectToAction("Index");
            //}
            //catch
            //{
                return View();
            //}
        }

    }
}